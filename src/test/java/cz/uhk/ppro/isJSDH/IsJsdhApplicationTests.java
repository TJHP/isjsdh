package cz.uhk.ppro.isJSDH;

import cz.uhk.ppro.isJSDH.model.Userr;
import cz.uhk.ppro.isJSDH.repository.UserrRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IsJsdhApplicationTests {

	@Autowired
	private UserrRepository userrRepository;

	@Test
	public void test() {
		Userr user = userrRepository.findByUsername("KejciP");
		assertEquals(user.getRole(),"ADMIN");
	}

}
