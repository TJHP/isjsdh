package cz.uhk.ppro.isJSDH.model;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "events", schema = "ppro_isjsdh")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "tittle")
    @Size(min=4, max=100)
    private String tittle;

    @NotNull
    @Column(name = "text")
    @Size(min=4, max=200)
    private String text;

    @NotNull
    @Column(name = "place")
    @Size(min=4, max=45)
    private String place;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateofaccident")
    private Date dateofaccident;

    public Event(){
    }

    public Event(@NotNull Long id, @NotNull String tittle, @NotNull String text, @NotNull String place, @NotNull Date dateofaccident ){
        this.id = id;
        this.tittle = tittle;
        this.text = text;
        this.place = place;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getDateofaccident() {
        return dateofaccident;
    }

    public void setDateofaccident(Date dateofaccident) {
        this.dateofaccident = dateofaccident;
    }
}
