package cz.uhk.ppro.isJSDH.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "specializations", schema = "ppro_isjsdh")
public class Specialization implements Serializable {

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "tittle")
    private String nazev;


    @OneToMany(mappedBy="specialization")
    private List<Member> mmembers;

    public Specialization(){

    }

    public Specialization(Long id, String nazev) {
        this.id = id;
        this.nazev = nazev;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
}
