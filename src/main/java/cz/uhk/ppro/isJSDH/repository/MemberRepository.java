package cz.uhk.ppro.isJSDH.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import cz.uhk.ppro.isJSDH.model.Member;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long>{

}