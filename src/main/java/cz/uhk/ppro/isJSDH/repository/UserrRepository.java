package cz.uhk.ppro.isJSDH.repository;

import cz.uhk.ppro.isJSDH.model.Userr;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserrRepository extends JpaRepository<Userr, Long> {
    Userr findByUsername(String username);
}