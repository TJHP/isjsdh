package cz.uhk.ppro.isJSDH.repository;

import cz.uhk.ppro.isJSDH.model.InterNew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterNewRepository extends JpaRepository<InterNew, Long> {

}