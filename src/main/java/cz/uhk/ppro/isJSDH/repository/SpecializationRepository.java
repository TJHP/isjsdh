package cz.uhk.ppro.isJSDH.repository;

import cz.uhk.ppro.isJSDH.model.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

    @Repository
    public interface SpecializationRepository extends JpaRepository<Specialization, Long> {

    }

