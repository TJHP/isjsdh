package cz.uhk.ppro.isJSDH.repository;

import cz.uhk.ppro.isJSDH.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}