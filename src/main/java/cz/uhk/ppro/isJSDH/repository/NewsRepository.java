package cz.uhk.ppro.isJSDH.repository;

import cz.uhk.ppro.isJSDH.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
}