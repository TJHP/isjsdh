package cz.uhk.ppro.isJSDH;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication(scanBasePackages = { "cz.uhk.ppro.isJSDH.controller", "cz.uhk.ppro.isJSDH.repository"
											,"cz.uhk.ppro.isJSDH.model", "cz.uhk.ppro.isJSDH.util"
									     	, "cz.uhk.ppro.isJSDH.exception"
											} )
@EnableJpaAuditing
public class IsJsdhApplication{


	public static void main(String[] args) {
		SpringApplication.run(IsJsdhApplication.class, args);
	}


}
