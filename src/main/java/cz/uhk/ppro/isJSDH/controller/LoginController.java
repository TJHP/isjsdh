package cz.uhk.ppro.isJSDH.controller;

import cz.uhk.ppro.isJSDH.model.Userr;
import cz.uhk.ppro.isJSDH.util.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Neplatne heslo nebo email.");

        if (logout != null)
            model.addAttribute("message", "Uspesne odhlaseni.");

        return "login";
    }

    @ModelAttribute("userr")
    public Userr userRegistrationDto() {
        return new Userr();
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userr") @Valid Userr userForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);
        return "redirect:/userrs";
    }
}
