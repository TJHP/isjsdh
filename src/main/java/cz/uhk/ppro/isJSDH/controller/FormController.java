package cz.uhk.ppro.isJSDH.controller;

import cz.uhk.ppro.isJSDH.model.*;
import cz.uhk.ppro.isJSDH.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class FormController {

    /* Interní informace pro jednotku */

    @Autowired
    private InterNewRepository interNewRepository;

    @ModelAttribute("interInfo")
    public InterNew interNew() {
        return new InterNew();
    }

    @GetMapping("/formInter")
    public String toFormInter(){
        return "formInter";
    }

    @PostMapping("/formInter")
    public String saveFormInter(@ModelAttribute("interInfo") @Valid InterNew formInter, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formInter";
        }
        interNewRepository.save(formInter);
        return "redirect:/interInfo";
    }

    @GetMapping("/deleteInter/{interInfoId}")
    public String toDeleteInterInfo(@PathVariable("interInfoId") long interInfoId){
        interNewRepository.deleteById(interInfoId);
        return "redirect:/interInfo";
    }

    @GetMapping("/updateInterInfo/{id}")
    public String showUpdateInterInfo(@PathVariable("id") long id, Model model) {
        InterNew interInfo = interNewRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("interrInfo", interInfo);
        return "formUpdateInterInfo";
    }

    @PostMapping("/updateInterInfo/{id}")
    public String showUpdatedInterNews(@PathVariable("id") long id, @ModelAttribute("interrInfo") @Valid  InterNew interNew, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formUpdateInterInfo";
        }
        interNewRepository.save(interNew);
        return "redirect:/interInfo";
    }

    /* Novinky */

    @Autowired
    private NewsRepository newsRepository;

    @ModelAttribute("news")
    public News news() {
        return new News();
    }

    @GetMapping("/formNews")
    public String toFormNews(){
        return "formNews";
    }

    @PostMapping("/formNews")
    public String registration(@ModelAttribute("news") @Valid News newsForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formNews";
        }
        newsRepository.save(newsForm);
        return "redirect:/news";
    }

    @GetMapping("/deleteNew/{newsId}")
    public String toDeleteNews(@PathVariable("newsId") long newsId){
        newsRepository.deleteById(newsId);
        return "redirect:/news";
    }

    @GetMapping("/updateNew/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        News news = newsRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("newss", news);
        return "formUpdateNew";
    }

    @PostMapping("/updateNew/{id}")
    public String showUpdate(@ModelAttribute("newss") @Valid News newsForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formUpdateNew";
        }
        newsRepository.save(newsForm);

        return "redirect:/news";
    }

    /* Události */

    @Autowired
    EventRepository eventRepository;

    @ModelAttribute("event")
    public Event event() {
        return new Event();
    }

    @GetMapping("/formEvents")
     public String toFormEvents(){
      return "formEvents";
  }

    @PostMapping("/formEvents")
    public String registrationEvent(@ModelAttribute("event") @Valid Event eventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formEvents";
        }

        eventRepository.save(eventForm);
        return "redirect:/events";
    }

    @GetMapping("/deleteEvent/{eventId}")
    public String toDeleteEvent(@PathVariable("eventId") long eventId){
        eventRepository.deleteById(eventId);
        return "redirect:/events";
    }

    @GetMapping("/updateEvent/{id}")
    public String showUpdateFormEvent(@PathVariable("id") long id, Model model) {
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("event", event);
        return "formUpdateEvent";
    }

    @PostMapping("/updateEvent/{id}")
    public String updatedEvent(@PathVariable("id") long id, @ModelAttribute("event") @Valid Event event, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formUpdateEvent";
        }
        eventRepository.save(event);
        return "redirect:/events";
    }

     /* Členové jednotky */

    @Autowired
    private MemberRepository memberRepository;

    @ModelAttribute("member")
    public Member member() {
        return new Member();
    }

    @GetMapping("/formMembers")
    public String toFormMembers(){
        return "formMembers";
    }

    @PostMapping("/formMembers")
    public String toSaveFormMembers(@ModelAttribute("member") @Valid Member member, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "formMembers";
        }
        memberRepository.save(member);
        return "redirect:/members";
    }

    @GetMapping("/deleteMember/{memberId}")
    public String toDeleteMember(@PathVariable("memberId") long memberId){
        memberRepository.deleteById(memberId);
        return "redirect:/members";
    }

    @GetMapping("/updateMember/{id}")
    public String updateMember(@PathVariable("id") long id, Model model) {
        Member member = memberRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("member", member);
        return "formUpdateMember";
    }

    @PostMapping("/updateMember/{id}")
    public String updatedMember(@PathVariable("id") long id, @ModelAttribute("member") @Valid Member member, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "formUpdateMember";
        }
        memberRepository.save(member);
        return "redirect:/members";
    }

    /* Uživatel */

    @Autowired
    UserrRepository userRepository;

    @GetMapping("/deleteUserr/{userId}")
    public String toDeleteUserr(@PathVariable("userId") long userId){
        userRepository.deleteById(userId);
        return "redirect:/userrs";
    }

}
