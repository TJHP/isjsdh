package cz.uhk.ppro.isJSDH.controller;

import cz.uhk.ppro.isJSDH.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {

    @RequestMapping("/")
    public String toMainPage(){ return "index"; }

    @Autowired
    NewsRepository newsRepository;

    @GetMapping("/news")
    public String getNews(Model model){
        model.addAttribute("listOfNewes", newsRepository.findAll());
        return "news";
    }

    @RequestMapping("/contact")
    public String toContact(){
        return "contact";
    }

    @Autowired
    MemberRepository memberRepository;

    @GetMapping("/members")
    public String getAllMembers(Model model){
        model.addAttribute("listOfMembers", memberRepository.findAll());
        return "members";
    }

    @Autowired
    InterNewRepository interNewRepository;

    @GetMapping("/interInfo")
    public String getAllinterInfos(Model model){
        model.addAttribute("listofinters", interNewRepository.findAll());
        return "interInfo";
    }

    @Autowired
    EventRepository eventRepository;

    @GetMapping("/events")
    public String getAllEvents(Model model){
        model.addAttribute("listOfEvents", eventRepository.findAll());
        return "events";
    }

    @Autowired
    UserrRepository userrRepository;

    @GetMapping("/userrs")
    public String getAllUserrs(Model model){
        model.addAttribute("listOfUserrs", userrRepository.findAll());
        return "userrs";
    }

    @GetMapping("/errorPage")
    public String toError(){
        return "errorPage";
    }
}
