package cz.uhk.ppro.isJSDH.util;


import cz.uhk.ppro.isJSDH.model.Userr;
import cz.uhk.ppro.isJSDH.repository.UserrRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserrRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(Userr user) {
        if (user.getPassword() != null) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        userRepository.save(user);
    }

    @Override
    public Userr findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}