package cz.uhk.ppro.isJSDH.util;


import cz.uhk.ppro.isJSDH.model.Userr;

public interface UserService {
    void save(Userr user);
    Userr findByUsername(String username);
}