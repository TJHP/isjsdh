-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: ppro_isjsdh
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(100) NOT NULL,
  `text` varchar(200) NOT NULL,
  `place` varchar(45) NOT NULL,
  `dateofaccident` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Nehoda','Nehoda 2 osobních vozidel, bez zranění.','Humpolec, Jiřická','2020-01-02'),(2,'Požár','Požár travního prostu 20m2','Plačkov-Herálec','2020-01-07'),(5,'Planý poplach','Pálení klestí, nenahlášeno. Oheň pod kontrolou','Holušice, nájezd na D1','2020-01-19'),(8,'Technická pomoc','Technická pomoc při odstraňování padlého stromu v ul. Hálkova.','Humpolec - Hálkova','2020-01-04');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interinfo`
--

DROP TABLE IF EXISTS `interinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `interinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interinfo`
--

LOCK TABLES `interinfo` WRITE;
/*!40000 ALTER TABLE `interinfo` DISABLE KEYS */;
INSERT INTO `interinfo` VALUES (1,'Školení jednotky JSDH v pondělí 20.1 2010'),(2,'Brigáda na úklid hasičárny pátek 24.1 2010'),(16,'Připomenutí zákazu kouření v prostorách hasičské zbrojnice!');
/*!40000 ALTER TABLE `interinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `specializationid` int(11) NOT NULL,
  `birthdate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specializationid` (`specializationid`) /*!80000 INVISIBLE */,
  CONSTRAINT `specializationid` FOREIGN KEY (`specializationid`) REFERENCES `specializations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'Pavel','Krejčí',1,'1978-11-18'),(2,'Martin ','Vondráček',3,'1996-12-31'),(36,'Jan','Vondráček',5,'2020-10-13'),(42,'Martin','Maršík',1,'1994-11-20'),(43,'Zdeněk','Kříž',2,'1994-02-10'),(44,'Tomáš ','Fučík',4,'1995-05-01');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(2000) NOT NULL,
  `datum` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (2,'Upozornění: pálení klestí je nutno nahlásit předem na www.hzs.cz','0000-00-00 00:00:00.000000'),(3,'SDH Humpolec a SVČ Humpolec zve rodiče s dětmi na tradiční akci Huhuliáda 2020 která se koná 21.1 2020 v prostorách SVČ.','2020-01-24 14:41:43.213000'),(12,'JSDH Humpolec vás zve na ukázku vyprošťování, které proběhne za účasti složek HZS dne 30.1 2020 na horním náměstí v Humpolci.','2020-01-24 20:09:20.861000'),(13,'Proběhla volba nového výboru SDH Humpolec. Zvolen nový starosta sboru Jaroslav Vašíček, který ve funkci nahradil dlouholetého starostu p. Františka Dvořáka.','2020-01-25 00:49:52.602000');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specializations`
--

DROP TABLE IF EXISTS `specializations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `specializations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(45) NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specializations`
--

LOCK TABLES `specializations` WRITE;
/*!40000 ALTER TABLE `specializations` DISABLE KEYS */;
INSERT INTO `specializations` VALUES (1,'Velitel družstva',''),(2,'Strojník',''),(3,'Hasič',''),(4,'Zdravotník',''),(5,'Preventista','');
/*!40000 ALTER TABLE `specializations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userr`
--

DROP TABLE IF EXISTS `userr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(500) NOT NULL,
  `realname` varchar(45) NOT NULL,
  `rolee` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userr`
--

LOCK TABLES `userr` WRITE;
/*!40000 ALTER TABLE `userr` DISABLE KEYS */;
INSERT INTO `userr` VALUES (19,'pohorto1','$2a$10$.FTTxx6nZUAcr/5.XW5T7OwDyFQpaGLat6c1jHaAz6kcTuA16YGEa','Tomáš Pohořský','HASIC'),(20,'krejcipa1','$2a$10$sC..gMMQUUNTiApG8BmQmuhn.qZr.8S5hOkXaDcd0rUfqy8JExM4C','Pavel Krejčí','ADMIN'),(21,'neubauerja1','$2a$10$2m8tgDM.hDWRHgC.PIFAx..no0HOJQElQyucsEBvyqwB72e3vTOb6','Jaroslav Neubaurer','VELITEL');
/*!40000 ALTER TABLE `userr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ppro_isjsdh'
--

--
-- Dumping routines for database 'ppro_isjsdh'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-25  2:13:04
